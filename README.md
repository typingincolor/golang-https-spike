# Golang https spike

Can I do https termination in golang - YES!

## Generating private key and certificate

```bash
openssl genrsa -out private_key 2048
openssl req -new -x509 -key private_key -out certificate -days 365
```

## Test request

```bash
curl --insecure \
     -H "custom: value" \
     -H "Accept: application/json" \
     "https://localhost:8443/products/1?a=b&c=d"
```

## Create a product
```bash
curl --insecure \
     -X POST \
     -d '{ "name": "mega widget"}' \
     -H "Content-Type: application/json" \
     https://localhost:8443/products
```
