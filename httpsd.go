package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

const (
	port        = ":8443"         // ssl port
	certificate = "./certificate" // openssl req -new -x509 -key private_key -out certificate -days 365
	privateKey  = "./private_key" // openssl genrsa -out private_key 2048
)

type product struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

func productQueryHandler(w http.ResponseWriter, request *http.Request) {
	logDebugInfo(request)

	// build response
	fmt.Fprint(w, "OK\n\r")
}

func productCreateHandler(w http.ResponseWriter, request *http.Request) {
	log.Print("Creating a Product...")
	var newProduct product

	decoder := json.NewDecoder(request.Body)
	err := decoder.Decode(&newProduct)

	if err != nil {
		log.Panic(err)
	}

	newProduct.ID = rand.Int()

	log.Printf("new product id: %d, name: %q", newProduct.ID, newProduct.Name)

	// build response
	jsonResult, _ := json.MarshalIndent(newProduct, "", "  ")
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprint(w, string(jsonResult))
}

func logDebugInfo(request *http.Request) {
	log.Printf("%q product id: %q", request.Method, mux.Vars(request)["key"])
	headers := request.Header

	log.Print("---- Headers -----")
	for k := range headers {
		log.Printf("%q:%q\n", k, headers[k])
	}

	log.Print("---- Query Parameters ----")
	queryParams := request.URL.Query()
	for param := range queryParams {
		log.Printf("%q:%q\n", param, queryParams[param])
	}
}

func main() {
	rand.Seed(time.Now().Unix())
	r := mux.NewRouter()
	r.HandleFunc("/products/{key}", productQueryHandler).Methods("GET")
	r.HandleFunc("/products", productCreateHandler).Methods("POST").Headers("Content-Type", "application/json")

	http.Handle("/", r)
	log.Print("main(): waiting...")

	err := http.ListenAndServeTLS(port, certificate, privateKey, nil)
	if err != nil {
		log.Fatalf("main(): %s\n", err)
	}
}
